# multiple words = _2fQ3Xj8U _2QBU0YsX
# single word = _1btShz4h

# ?langFrom=es spanish -> english
# ?langFrom=en english -> spanish

# BeautifulSoup tutorial: https://realpython.com/beautiful-soup-web-scraper-python/

from typing import NamedTuple
import requests
import itertools
from bs4 import BeautifulSoup

class Card(NamedTuple):
    english: str
    spanish: str
    pronunciation=None
    image=None

    def __str__(self):
        return "{}  {}  {}  {}".format(self.english, self.spanish, self.pronunciation, self.image)

def scrape_translation(word, lang='en'):
    search_bytes = word.encode()
    hex_bytes = [hex(byte)[2:] for byte in search_bytes]
    search_url = "".join((word[i] if word[i] in "abcdefghijklmnopqrstuvwxyz" else '%' + hex_bytes[i] for i in range(len(word))))
    
    URL = "https://www.spanishdict.com/translate/{}?langFrom={}".format(search_url, lang)

    page = requests.get(URL)
    soup = BeautifulSoup(page.content, "html.parser")

    definitions = []
    for i in itertools.count(1, 1):
        definitions.append(soup.find(id="quickdef{}-{}".format(i, lang)))
        if definitions[-1] == None:
            definitions.pop()
            break

    if len(definitions) == 0:
        raise Exception("No results found")

    definitions = [definition.find(class_="_1btShz4h").text for definition in definitions]

    return definitions

cards = []

lang = input("What language will you be entering? [en/es] ").strip()

try:
    while True:
        word = input("Search term: ").strip()
        if lang == "es":
            es_word = word
            en_translation = scrape_translation(word, lang="es")
            word = en_translation[0]
        elif lang == "en":
            en_word = word
            es_translation = scrape_translation(word, lang="en")
            word = es_translation[0]

        if lang == "en":
            es_word = word
            en_translation = scrape_translation(word, lang="es")
            word = en_translation[0]
        elif lang == "es":
            en_word = word
            es_translation = scrape_translation(word, lang="en")
            word = es_translation[0]


        cards.append(Card(en_word, ", ".join(es_translation)))
        cards.append(Card(es_word, ", ".join(en_translation)))
except:
    pass

def cards_to_file(filepath, cards):
    with open(filepath, "w") as file:
        for card in cards:
            file.write(str(card) + "\n")


print([str(card) for card in cards])
cards_to_file("test.txt", cards)